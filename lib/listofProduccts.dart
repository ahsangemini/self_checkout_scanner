import 'package:checking_barcode/models/order.dart';
import 'package:checking_barcode/screens/orderDetails.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class CartTotal extends StatelessWidget {
  CartTotal({Key key, @required this.orderid}) : super(key: key);
  final String orderid;
  OrderModel _orderModel;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder<OrderModel>(
        future: Future.wait([
          Firestore.instance.collection("orders").document(orderid).get(),
        ]).then(
          (response) => _orderModel = OrderModel.fromFirestore(
            response[0],
          ),
        ),
        builder: (context, AsyncSnapshot<OrderModel> snapshot) {
          if (snapshot.data == null) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return OrderDetails(
              order: snapshot.data,
            );
          }
        },
      ),
    );
  }
}
