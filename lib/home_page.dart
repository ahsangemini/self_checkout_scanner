import 'dart:developer';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:checking_barcode/auth.dart';
import 'package:checking_barcode/listofProduccts.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class HomePage extends StatelessWidget {
  HomePage({this.auth, this.onSignOut});
  final BaseAuth auth;
  final VoidCallback onSignOut;
  ScanResult _scanResult;

  static final _possibleFormats = BarcodeFormat.values.toList()
    ..removeWhere((e) => e == BarcodeFormat.unknown);
  List<BarcodeFormat> selectedFormats = _possibleFormats;
  // This widget is the root of your application.

  Future scan() async {
    try {
      var options = ScanOptions(
        restrictFormat: selectedFormats,
        useCamera: -1,
        autoEnableFlash: false,
      );

      var result = await BarcodeScanner.scan(options: options);

      _scanResult = result;
    } on PlatformException catch (e) {
      var result = ScanResult(
        type: ResultType.Error,
        format: BarcodeFormat.unknown,
      );

      if (e.code == BarcodeScanner.cameraAccessDenied) {
        result.rawContent = 'The user did not grant the camera permission!';
      } else {
        result.rawContent = 'Unknown error: $e';
      }
      _scanResult = result;
    }
  }

  @override
  Widget build(BuildContext context) {
    void _signOut() async {
      try {
        await auth.signOut();
        onSignOut();
      } catch (e) {
        print(e);
      }
    }

    return new Scaffold(
      appBar: new AppBar(
        actions: <Widget>[
          new FlatButton(
              onPressed: _signOut,
              child: new Text('Logout',
                  style: new TextStyle(fontSize: 17.0, color: Colors.white)))
        ],
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Text(
          'Scan The Bill From the Button',
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await scan();
          print(_scanResult.rawContent);
          if (_scanResult.rawContent != null) {
            log(_scanResult.rawContent.toString() + " AhsanAli");
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => CartTotal(
                          orderid: _scanResult.rawContent,
                        )));
          }
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
